<!--
.. title: Il Catalogo dei Libri Liberi
.. slug: il-catalogo-dei-libri-liberi
.. date: 2014-09-19 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Disponibile online la nuova edizione del "Catalogo libri di testo e manualistica liberamente disponibili in rete", ricco indice di testi di carattere didattico ed in lingua italiana rilasciati in licenza libera.

Il catalogo, periodicamente aggiornato con le ultime novità del settore, è diviso per materie ed offre svariati spunti dedicati alle scuole: i volumi classificati sono gratuitamente fruibili in formato digitale (e dunque eventualmente stampabili a basso costo presso una qualsiasi tipografia locale) e, essendo distribuiti con licenza aperta, permettono ai docenti che li volessero adottare in classe di modificarli, estrarne porzioni più o meno grandi, ed insomma adattarli al proprio personale programma, per l'intera durata del corso o per una singola lezione. Nel primo paragrafo sono inoltre indicate diverse altre fonti di contenuti - rigorosamente liberi - utili all'insegnamento.

Invitiamo tutti, e soprattutto insegnanti, genitori e studenti, a consultare l'indice alla ricerca di materiali che possano essere utilizzati in aula o, più semplicemente, letti per propria cultura personale. Perché la libera conoscenza è e deve essere alla portata di tutti, ragazzi ed adulti.

Il file può essere scaricato <a rel="nofollow" href="http://creativecommons.it/Education">dal sito di Creative Commons Italia</a> (più precisamente <a rel="nofollow" href="http://creativecommons.it/ccitfiles/catalogo-testi-cc.pdf">qui</a>).
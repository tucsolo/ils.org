<!--
.. title: Premio Jacopo Crivelli per il Linux Day 2020
.. slug: premio-jacopo-crivelli-per-il-linux-day-2020
.. date: 2020-10-08 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: <a rel="nofollow" href="https://commons.wikimedia.org/wiki/File:File-_Facade_of_the_cathedral_is_Florence.jpg">PTG Dudva</a>, <a rel="nofollow" href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons
.. previewimage: /images/posts/crivelli.jpg
-->


Di seguito un comunicato che annuncia una brutta notizia ma anche una bella iniziativa: Jacopo Crivelli, socio fondatore di Libera Informatica (l'associazione da cui nasce la <a href="/sezionilocali/firenze">Sezione Locale ILS di Firenze</a>), e' recentemente mancato in circostanze tragiche, e per rispondere a questa improvvisa perdita gli amici toscani stanno predisponendo un premio intitolato alla sua memoria da destinare ad uno dei talk del <a href="https://www.linuxday.it/2020/">Linux Day 2020</a>.

<!-- TEASER_END -->

Nell'esprimere il nostro cordoglio per questa perdita, ringraziamo i colleghi fiorentini per l'opportunità data ai relatori della manifestazione nazionale e per aver scelto questo modo per ricordare il loro (e un po' di tutti noi) amico.

<blockquote>
Il 27 giugno 2020 è improvvisamente mancato Jacopo "Jack" Crivelli, socio fondatore delle associazioni Libera Informatica e Restarters Firenze.<br>
Jack, perito elettronico, aveva le mani d'oro. Nelle case dei suoi numerosi amici, e non solo, c'è almeno un oggetto riparato da lui, o un’installazione di Software Libero.<br>
Quasi 15 anni fa incontra il Software Libero attraverso il trashware. Negli anni successivi partecipa attivamente alla vita delle sue associazioni e diventa punto di riferimento per tutti noi, nell'ambito elettronico e informatico.<br>
La sezione locale di Firenze intitola alla sua memoria un premio in denaro.<br>
Sarà assegnato a uno dei progetti o iniziative presenti nel programma del Linux Day 2020; sceglieremo fra questi quello che pensiamo Jack avrebbe maggiormente apprezzato.<br>
<br>
Chi desidera contribuire al premio può inviare <a href="/info#donazioni">un bonifico a Italian Linux Society</a> con causale "Premio Jacopo Crivelli" :<br>
Banca: Unicredit Banca<br>
IBAN: IT 74 G 02008 12609 000100129899<br>
Intestato a: ILS ITALIAN LINUX SOCIETY<br>
<br>
Assegneremo il premio nei giorni successivi al Linux Day. La giuria sarà composta da soci e amici di Jacopo.<br>
<br>
Grazie, saluti e buon Linux Day!<br>
Gianna Papi, reference della Sezione Locale ILS di Firenze
</blockquote>
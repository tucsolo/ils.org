<!--
.. title: Assemblea ILS 2019
.. slug: assemblea-ils-2019
.. date: 2019-04-09 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Dopo l'esperienza dello scorso anno a Torino nel contesto di <a rel="nofollow" href="https://merge-it.net/">MERGE-it</a>, anche quest'anno l'assemblea dei soci ILS sarà tenuta in una grande città facilmente raggiungibile da tutti e sarà pubblica ed aperta a tutti coloro che vorranno partecipare.

In particolare l'ordine del giorno prevede un ampio spazio dedicato alla questione delle Sezioni Locali ILS, introdotta <a href="{% link _posts/2018-03-29-report-lugconf-2018.md %}">in occasione della scorsa LUGConf</a> e che ha suscitato negli ultimi mesi interesse tra diversi Linux Users Groups italiani: l'intento è quello di far convergere in Italian Linux Society le associazioni ed i gruppi informali locali di promozione e supporto a Linux, al fine di accentrare e semplificare gli aspetti burocratici ed amministrativi pur mantendendo lo status di associazione e godendo dei vantaggi offerti dalla Riforma del Terzo Settore. I rappresentanti dei LUG interessati a questa opzione sono invitati a venire a discutere con noi modalità, vincoli e opportunità di tale iniziativa, al fine di meglio valutare la possibilità di aderirvi nel prossimo futuro.

L'appuntamento è dunque per <strong>sabato 27 aprile 2019</strong> alle ore 14:30 presso lo Sportello Eco-Equo del centro Le Murate, in <a rel="nofollow" href="https://www.openstreetmap.org/?mlat=43.76928&mlon=11.26916#map=19/43.76928/11.26916">Via dell'Agnolo 1/d, <strong>Firenze</strong></a>. La partecipazione è aperta, ma chi volesse aggregarsi anche al pranzo che si terrà prima dell'assemblea è invitato a scrivere a <a href="mailto:ils-cd@linux.it">ils-cd@linux.it</a> per segnalare la propria presenza e prenotarsi.
<!--
.. title: MERGE-it 2023
.. slug: mergeit-2023
.. date: 2023-05-10 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage: /images/posts/mergeit.png
-->


Venerdi 12 e sabato 13 maggio si svolge a Verona [MERGE-it](https://merge-it.net/) 2023, la conferenza che raduna le realtà italiane che operano nell'ambito delle libertà digitali.

<!-- TEASER_END -->

L'evento, nato a Torino nel 2018 e reiterato ad intervalli irregolari, è rivolto in prima istanza agli "addetti ai lavori" che già aderiscono ad una o più delle numerose realtà (locali o nazionali) e vogliono saperne di più su cosa succede a livello italiano, ma anche a chi vorrebbe iniziare a prendere parte attiva e desidera orientarsi tra i temi, le iniziative, le realtà, e le opportunità che esistono per poter contribuire in modo pratico alla diffusione ed alla crescita del software libero e della cultura aperta nel nostro Paese.

In questa terza edizione della conferenza, ampio spazio (l'intera giornata di venerdi 12) è stato dedicato ai soggetti professionali ed imprenditoriali operanti con soluzioni libere e open source, spesso troppo poco conosciute dal mondo community eppure indispensabili per perseguire gli obiettivi comuni più ambiziosi (come ad esempio l'adozione del software libero da parte delle pubbliche amministrazioni).

Ma MERGE-it resta comunque un evento organizzato dalle community e dedicato alle community, ed in particolare nella giornata di sabato 13 si svolgeranno una serie di dibattiti aperti su temi particolarmente trasversalmente sentiti e cari: la scuola, i giovani, e la comunicazione. Ciascuna sessione aspira a raccogliere esperienze e considerazioni, ma soprattutto progetti e propositi operativi da parte dei partecipanti.

Il programma ufficiale prevede anche un appuntamento di benvenuto per giovedi sera, dedicato a chi arriva da fuori città, ed attività pratiche per la domenica, per chi non vuole stare troppo a lungo seduto a parlare e ascoltare ma vuole un poco "sporcarsi le mani". Tutti i dettagli sono [sul sito](https://merge-it.net/).